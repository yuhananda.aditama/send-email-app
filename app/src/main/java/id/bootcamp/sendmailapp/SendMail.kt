package id.bootcamp.sendmailapp

import android.content.Context
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Properties
import javax.mail.Authenticator
import javax.mail.Message
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage


class SendMail(private val context: Context) {
    private val email = "java.batch297@gmail.com"
    private val password = "yrprjhrwvvbztoqk"

    private val subject = "Salam dari Batch 293"
    private val message = "Pakde Bintang, ajari cara make aplikasi hijau?"

    fun sendEmail() {
        CoroutineScope(Dispatchers.IO).launch {
            val props = Properties()
            props.put("mail.smtp.host", "smtp.gmail.com")
            props.put("mail.smtp.socketFactory.port", "465")
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory")
            props.put("mail.smtp.auth", "true")
            props.put("mail.smtp.port", "465")
            val session = Session.getDefaultInstance(props,object : Authenticator(){
                override fun getPasswordAuthentication(): PasswordAuthentication {
                    return PasswordAuthentication(email,password)
                }
            })
            try {
                val mm = MimeMessage(session)
                mm.setFrom(InternetAddress(email))
                mm.addRecipient(Message.RecipientType.TO, InternetAddress("bintangkputra@gmail.com"))
                mm.subject = subject
                mm.setText(message)
                Transport.send(mm)
            }catch (e:Exception){
                e.printStackTrace()
            }
        }
    }

}