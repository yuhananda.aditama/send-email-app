package id.bootcamp.sendmailapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.bootcamp.sendmailapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnEmail.setOnClickListener {
            val mail = SendMail(this)
            mail.sendEmail()
        }
    }
}